#%Module1.0

module-whatis		"an accelerator modeling code"
module-url		"http://www.aps.anl.gov/Accelerator_Systems_Division/Accelerator_Operations_Physics/elegant.html"
module-license		"See: http://www.aps.anl.gov/epics/license/index.php"
module-maintainer	"Achim Gsell <achim.gsell@psi.ch>"

module-help	"
elegant is an accelerator modeling code that performs many functions. Its
basic function is 6D tracking, which it performs using matrices (up to
second order), symplectic integration, numerical integration, or a user-
defined mixture. It computes Twiss parameters, transport matrices, radiation
integrals, correction matrices, amplification factors, and floor coordinates.
It also performs optimization, including optimization of radiation integrals,
floor coordinates, transport matrices, and beam properties from tracking. A
number of time-dependent elements are supported, such as rf cavities, kickers,
and ramping machines. The code provides simulation of various collective
effects, such as wakes and coherent synchrotron radiation.
"

setenv		RPN_DEFNS		$PREFIX/RPN_DEFNS
setenv		HOST_ARCH		linux-x86_64
setenv		EPICS_HOST_ARCH		linux-x86_64
setenv		OAG_TOP_DIR		$PREFIX

append-path	PATH			$PREFIX/epics/extensions/bin/linux-x86_64
append-path	PATH			$PREFIX/oag/apps/bin/linux-x86_64

append-path	LIBRARY_PATH		$PREFIX/epics/extensions/lib/linux-x86_64
append-path	C_INCLUDE_PATH		$PREFIX/epics/extensions/include
append-path	CPLUS_INCLUDE_PATH	$PREFIX/epics/extensions/include
append-path	PKG_CONFIG_PATH		$PREFIX/epics/base/lib
